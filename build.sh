#!/bin/sh
set -e

# First argument has to be the desired modbus version
MODBUS_VERSION=$1

# Second argument is the install location
RELEASE_DIR=$2

# Third argument has to be the desired build target (e.g. x86_64)
BUILD_TARGET=$3

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in
MODBUS_NAME=libmodbus-${MODBUS_VERSION}
MODBUS_FOLDER_PATH=${SCRIPTPATH}/libmodbus-${MODBUS_VERSION}-${BUILD_TARGET}

echo ${MODBUS_FOLDER_PATH}

# If there is no modbus folder yet, we download the required version and build it
if [ -d ${MODBUS_FOLDER_PATH} ]; then
  echo "modbus version is already available. No need to download it"
else
  
  archiveFile=${MODBUS_NAME}.tar.gz
  archiveFilePath=${SCRIPTPATH}/${archiveFile}

  if [ ! -f "$archiveFilePath" ]
  then
     echo "Downloading modbus"
     cd ${SCRIPTPATH}
     curl -L https://github.com/stephane/libmodbus/releases/download/v${MODBUS_VERSION}/libmodbus-${MODBUS_VERSION}.tar.gz  > libmodbus-${MODBUS_VERSION}.tar.gz
  fi

  if [ ! -d ${MODBUS_FOLDER_PATH} ]
  then
     echo "Extracting modbus"
     tar -vxzf ${archiveFilePath}
     mv ${MODBUS_NAME} ${MODBUS_FOLDER_PATH}
  fi
fi

echo "Building and installing modbus"
  
cd ${MODBUS_FOLDER_PATH}
if [ ${BUILD_TARGET} == yocto-linux ]; then
  # Following manual from https://docs.yoctoproject.org/2.0.3/adt-manual/adt-manual.html#creating-and-running-a-project-based-on-gnu-autotools
  aclocal
  autoconf
  touch NEWS README AUTHORS ChangeLog
  automake -a
  ./configure --enable-static --prefix=${RELEASE_DIR} --exec_prefix=${RELEASE_DIR}/${BUILD_TARGET} ${CONFIGURE_FLAGS}
else
  ./configure --enable-static --prefix=${RELEASE_DIR} --exec_prefix=${RELEASE_DIR}/${BUILD_TARGET}
fi
make
make install
