#!/bin/sh
set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in
RELEASE_DIR=${SCRIPTPATH}/install

# x86_64 build
./install.sh ${RELEASE_DIR}

# yocto build
export YOCTO_SDK_ENV=/common/jenkins/yocto/fesa/environment-setup-core2-64-ffos-linux
BUILD_TARGET=yocto-linux ./install.sh ${RELEASE_DIR}
